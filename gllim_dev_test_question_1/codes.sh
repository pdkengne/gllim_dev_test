#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# Creation of a share library on linux.
#
# The create library is called "libdeterminantcpp.so"
#
# This library contains three functions to calculate the determinant of squared matrices of orders two, three and four, respectively.
# Three examples are also included for illustrations.



cd dev

cd build

cmake ..

make ./src/determinantcpp

make install

sudo make install


# Install the project...
# -- Install configuration: "Release"
# -- Installing: /usr/local/lib/libdeterminantcpp.so
# -- Installing: /usr/local/include/package/det_2d.hpp
# -- Installing: /usr/local/include/package/det_3d.hpp
# -- Installing: /usr/local/include/package/det_4d.hpp
# -- Installing: /usr/local/bin/package-example_det_2d
# -- Installing: /usr/local/bin/package-example_det_3d
# -- Installing: /usr/local/bin/package-example_det_4d


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

