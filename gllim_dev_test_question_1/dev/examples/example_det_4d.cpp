#include <iostream>
#include <cassert>
#include "det_4d.hpp"

int main()
{
  
  int mat[N][N] = { { 1, 0, 2, -1 },
  { 3, 0, 0, 5 },
  { 2, 1, 4, -3 },
  { 1, 0, 5, 0 } };
  
  // Function call
  printf("Determinant of the matrix is : %d",
         determinantOfMatrix(mat, N));
         assert(determinantOfMatrix(mat, N) == 30);
  return 0;
 }
