# importing the numpy package
# as np
import numpy as np

def determinant(mat):
  # calling the det() method
  det = np.linalg.det(mat)
  return round(det)
  
# Driver Code
# declaring the matrix
mat = [[1, 1, 2],
      [1, 2, 0],
      [1, 3, 1]]

# Function call
print('Determinant of the matrix is:', determinant(mat))
