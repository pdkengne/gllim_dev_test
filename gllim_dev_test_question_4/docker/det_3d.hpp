#pragma once

// Dimension of input square matrix
#define N 3

int determinantOfMatrix(int mat[N][N], int n);
