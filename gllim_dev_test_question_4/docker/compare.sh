
# Comparing to the C++ and the Python implementations of the determinant

# Example 1
# C++ implementation
time package-example_det_4d
# Python implementation
time python example_det_4d.py

# Example 2
# C++ implementation
time package-example_det_3d
# Python implementation
time python example_det_3d.py

# Example 3
# C++ implementation
time package-example_det_2d
# Python implementation
time python example_det_2d.py
