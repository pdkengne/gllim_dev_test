#pragma once

// Dimension of input square matrix
#define N 4

int determinantOfMatrix(int mat[N][N], int n);
