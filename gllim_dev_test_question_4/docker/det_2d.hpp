#pragma once

// Dimension of input square matrix
#define N 2

int determinantOfMatrix(int mat[N][N], int n);
