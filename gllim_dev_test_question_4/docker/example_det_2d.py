# importing the numpy package
# as np
import numpy as np

def determinant(mat):
  # calling the det() method
  det = np.linalg.det(mat)
  return round(det)
  
# Driver Code
# declaring the matrix
mat = [[2,1],
      [1,4]]

# Function call
print('Determinant of the matrix is:', determinant(mat))
